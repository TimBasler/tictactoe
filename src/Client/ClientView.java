package Client;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;
import javafx.stage.Stage;

public class ClientView{
	protected Stage stage;
	protected ClientModel model;
	
	protected BorderPane root;
	protected HBox topBox,bottomBox;
	protected Label nameLabel, portLabel,IpAdressLabel;
	protected TextField nameFied, portField, IpAdressField,chatMessagesField;
	protected Button connectButton, sendButton;
	protected TextArea chatTextArea;
	protected Region spacer;
	
	//Constructor
	public ClientView(Stage stage,ClientModel model ) {
		this.stage=stage;
		this.model=model;
		
		this.root= new BorderPane();
		
		//Top from the Borderpane
		this.topBox= new HBox();
		this.nameLabel= new Label("Name:");
		this.portLabel= new Label("Port:");
		this.IpAdressLabel= new Label("IpAdresse");
		this.nameFied= new TextField();
		this.portField= new TextField();
		this.IpAdressField= new TextField();
		this.connectButton= new Button("Connect");
		this.topBox.getChildren().addAll(this.nameLabel,this.nameFied,this.portLabel,this.portField,this.IpAdressLabel,this.IpAdressField, this.connectButton);
		this.root.setTop(this.topBox);
		
		//Center
		this.chatTextArea = new TextArea("Chat messages will be shown here");
		root.setCenter(this.chatTextArea);
		
		//Bottom
		this.bottomBox= new HBox();
		this.chatMessagesField= new TextField("Enter your Messages here");
		this.spacer = new Region(); //Empty Spacer
		this.sendButton = new Button("Send");
		this.bottomBox.getChildren().addAll(this.chatMessagesField,this.spacer,this.sendButton);
		this.root.setBottom(this.bottomBox);
		
		//Builds the Scene
		stage.setTitle("Client");
		Scene scene = new Scene(root);
		stage.setScene(scene);
	}

	//Displays the GUI
	public void start() {
		this.stage.show();
	}

}
