package Client;

import java.io.IOException;
import java.net.Socket;
import java.util.logging.Logger;

import javafx.beans.property.SimpleStringProperty;

public class ClientModel {

	protected SimpleStringProperty newestMessage = new SimpleStringProperty();
	protected Logger logger = Logger.getLogger("");

	protected String name;
	protected Socket socket;

	// Constructor
	public ClientModel() {
	}

	// Method to connect with Server
	public void connect(String name, int Port, String ipAdress) {
		logger.info("Connected");
		try {
			socket = new Socket(ipAdress, Port);
		} catch (Exception e) {
			logger.warning(e.toString());
		}
	}

	// Dissconect from the Server
	public void disconnect() {
		logger.info("Dissconect");
		if (socket != null)
			try {
				socket.close();
			} catch (IOException e) {
				// Uninteresting
			}
	}

	// Send messages
	public void sendMessage(String message) {
		logger.info("Send Messages");
	}

	// Receive messages
	public String receiveMessage() {
		logger.info("Receive Messages");
		return newestMessage.get();
	}

}
