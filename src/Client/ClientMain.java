package Client;

import javafx.application.Application;
import javafx.stage.Stage;

public class ClientMain extends Application {
	protected ClientModel model;
	protected ClientView view;
	protected ClientController controller;

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		this.model = new ClientModel();
		this.view = new ClientView(primaryStage, model );
		this.controller = new ClientController(model,view);
		
		//Starts the View from the Server
		view.start();
		
	}
	
}
