package Server;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;
import javafx.stage.Stage;

public class ServerView {
	protected Stage stage;
	protected ServerModel model;
	protected BorderPane root;
	protected Label portLabel;
	protected HBox topBox;
	protected TextField portTextField;
	public TextArea connectedClientsArea;
	protected Button startServerButton;
	protected Region spacer;
	protected ScrollPane scrollPane;

	// Constructor
	public ServerView(Stage stage, ServerModel model) {
		this.stage = stage;
		this.model = model;
		
		this.root = new BorderPane();

		// Creates the Top of the GUI
		this.connectedClientsArea= new TextArea("Hallo");
		this.topBox = new HBox();
		this.portLabel = new Label("Enter the Port Number here");
		this.portTextField = new TextField("");
		this.spacer = new Region();//Empty Spacer
		this.startServerButton = new Button("Start the Server");
		this.topBox.getChildren().addAll(this.portLabel,this.portTextField,this.spacer,this.startServerButton);
		root.setTop(this.topBox);

		// Area which shows de connected Clients
		    this.scrollPane = new ScrollPane();
		    this.connectedClientsArea = new TextArea("BSP");
	        scrollPane.setHbarPolicy(ScrollBarPolicy.NEVER);
	        scrollPane.setVbarPolicy(ScrollBarPolicy.AS_NEEDED);
	        scrollPane.setFitToHeight(true);
	        scrollPane.setFitToWidth(true);
	        root.setCenter(scrollPane);
	        scrollPane.setContent(this.connectedClientsArea);
	        portLabel.setWrapText(true);

		// Builds the Scene
		stage.setTitle("TicTacToe Server");
		Scene scene = new Scene(root);
		stage.setScene(scene);
	}

	// Displays the GUI
	public void start() {
		this.stage.show();
	}
	
	protected void updateClients() {
		StringBuffer sb = new StringBuffer();
		for (Client c : model.clientList) {
			sb.append(c.toString());
			sb.append("\n");
		}
		this.connectedClientsArea.setText(sb.toString());
	}

}
