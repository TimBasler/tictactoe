package Server;

import java.io.IOException;
import java.net.Socket;

public class Client {
	protected Socket socket;
	protected String name;
	protected ServerModel model;
	
	public Client (ServerModel model,Socket socket) {
		this.model=model;
		this.socket=socket;	
		
	}
	
	public void stop() {
		try {
			socket.close();
		}catch(IOException x) {
			//TODO Add code here
		}
	}
	
	public String toString() {
		return this.name +" :"+socket.toString();
	}
	
	

}
