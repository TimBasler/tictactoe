package Server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class ServerModel {
	protected ObservableList<Client> clientList = FXCollections.observableArrayList();

	protected Logger logger = Logger.getLogger("");
	protected ServerSocket listener;
	private volatile boolean stop = false;

	// Start the Serer
	public void startServer(int port) {
		logger.info("Start server");
		try {
			listener = new ServerSocket(port, 10, null);
			Runnable r = new Runnable() {
				@Override
				public void run() {
					while (!stop) {
						try {
							Socket socket = listener.accept();
							Client client = new Client(ServerModel.this, socket);
							clientList.add(client);
						} catch (Exception e) {
							logger.info(e.toString());
						}
					}
				}
			};
			Thread t = new Thread(r, "ServerSocket");
			t.start();
		} catch (IOException e) {
			logger.info(e.toString());
		}
	}

	// Stop the server
	public void stopServer() {
		logger.info("Stop all clients");
		for (Client c : clientList)
			c.stop();
		logger.info("Stop server");
		stop = true;
		if (listener != null) {
			try {
				listener.close();
			} catch (IOException e) {
				// Uninteresting
			}
		}
	}

	public ObservableList<Client> getClientList() {
		logger.info("Get list of the Clients");
		return clientList;
	}


	

}
