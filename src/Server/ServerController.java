package Server;

import Client.ClientModel;
import Client.ClientView;

public class ServerController {
	protected ServerModel model;
	protected ServerView view;
	
	public ServerController(ServerModel model,ServerView view) {
		this.model=model;
		this.view=view;
	}

}