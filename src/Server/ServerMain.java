package Server;

import javafx.application.Application;
import javafx.stage.Stage;

public class ServerMain extends Application {
	protected ServerModel model;
	protected ServerView view;
	protected ServerController controller;

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		this.model = new ServerModel();
		this.view = new ServerView(primaryStage, model);
		this.controller = new ServerController(model, view);

		// Starts the View from the Server
		view.start();

	}

}